from sanic import Sanic
from sanic.response import text

app = Sanic()

@app.route('/api/email/send', methods=['POST'])
async def email_send(request):

    obj = request.json

    from mailer import make_mail
    status = make_mail(obj)

    if status == 0:
        print('\n\nEmail sent successfully!\n\n')
        return text('\n\nEmail sent successfully!\n\n')
    else:
        print('\n\nNot sent!\n\n')
        return text('\n\nNot sent!\n\n')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
